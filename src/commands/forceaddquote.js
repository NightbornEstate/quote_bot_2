const {
    Command
} = require('discord-akairo');

const discord = require("discord.js")
const Message = discord.Message

class ForceaddquoteCommand extends Command {
    constructor() {
        super('forceaddquote', {
            aliases: ['forceaddquote'],
            category: "utility",
            channelRestriction: 'guild',
            args: [{
                id: "staff",
                type: "member"
            },{
                id: "quote",
                type: "string",
                match: "rest"
            }]
        });
    }
    
    /**
     * 
     * @param {Message} message Message sent
     * @param {*} param1 
     */
    async exec(message, { staff, quote }) {
        if (!staff) {
            return await message.channel.send(
                new discord.RichEmbed()
                    .setTitle("I couldn't find that member.")
                    .setDescription("Usage: `,forceaddquote @StaffMember#0001 Hello I am a staff member`")
                    .setColor(0x7ebdc3)
            )
        }
        var enable = await message.client.knex("enabled_staff").where({member_id: staff.id, guild_id: message.guild.id}).first()
        if (!enable) {
            return await message.channel.send(
                new discord.RichEmbed()
                    .setTitle("They don't have quotes enabled!")
                    .setDescription(`You can enable them with \`,enablequotes @${staff.user.username}#${staff.user.discriminator} trigger\`.`)
                    .setColor(0x7ebdc3)
            )
        }
        await message.client.knex("quotes")
            .insert({ member_id: staff.id, guild_id: message.guild.id, quote })
        await message.react("✅")
    }
}
module.exports = ForceaddquoteCommand;
