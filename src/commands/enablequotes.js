const {
    Command
} = require('discord-akairo');

const discord = require("discord.js")
const Message = discord.Message

class EvalCommand extends Command {
    constructor() {
        super('enablequotes', {
            channelRestriction: 'guild',
            aliases: ['enablequotes'],
            category: "utility",
            userPermissions: "ADMINISTRATOR",
            args: [{
                id: "staff",
                type: "member"
            }, {
                id: "trigger",
                type: "string"
            }]
        });
    }
    
    /**
     * 
     * @param {Message} message Message sent
     * @param {*} param1 
     */
    async exec(message, { staff, trigger }) {
        if (!trigger) {
            return await message.channel.send(
                new discord.RichEmbed()
                    .setTitle("Usage")
                    .setDescription("`,enablequotes @Person#1234 trigger`")
                    .setColor(0x7ebdc3)
            )
        }
        if (!staff) {
            return await message.channel.send(
                new discord.RichEmbed()
                    .setTitle("I couldn't find that member.")
                    .setDescription("Please try using a mention")
                    .setColor(0x7ebdc3)
            )
        }
        var previous_enable = await message.client.knex("enabled_staff").where({member_id: staff.id, guild_id: message.guild.id}).first()
        if (previous_enable) {
            return await message.channel.send(
                new discord.RichEmbed()
                    .setTitle("Member already has quotes enabled!")
                    .setDescription("Quotes are enabled under trigger `" + previous_enable.trigger + "`")
                    .setColor(0x7ebdc3)
            )
        }
        
        await message.client.knex("enabled_staff")
            .insert({ member_id: staff.id, guild_id: message.guild.id, trigger: trigger })
        return await message.channel.send(
            new discord.RichEmbed()
                .setTitle("Enabled quotes!")
                .setDescription("Quotes are now enabled under trigger `" + trigger + "`")
                .setColor(0x7ebdc3)
        )
    }
}
module.exports = EvalCommand;
