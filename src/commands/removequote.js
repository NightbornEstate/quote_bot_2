const {
    Command
} = require('discord-akairo');

const discord = require("discord.js")
const Message = discord.Message

class RemovequoteCommand extends Command {
    constructor() {
        super('removequote', {
            channelRestriction: 'guild',
            aliases: ['removequote'],
            category: "utility",
            args: [{
                id: "quote_id",
                type: "number"
            }]
        });
    }
    
    /**
     * 
     * @param {Message} message Message sent
     * @param {*} param1 
     */
    async exec(message, { quote_id }) {
        var enable = await message.client.knex("enabled_staff").where({member_id: message.member.id, guild_id: message.guild.id}).first()
        if (!enable) {
            return await message.channel.send(
                new discord.RichEmbed()
                    .setTitle("You don't have quotes enabled!")
                    .setDescription("Please ask an administrator.")
                    .setColor(0x7ebdc3)
            )
        }
        if (quote_id == undefined) {
            return await message.channel.send(
                new discord.RichEmbed()
                    .setTitle("I couldn't parse the quote number.")
                    .setDescription("Usage: `,removequote 5`")
                    .setColor(0x7ebdc3)
            )
        }
        var guild_id = message.guild.id
        var quote = await message.client.knex("quotes").where({ quote_id, guild_id }).first()
        if (!quote) {
            return await message.channel.send(
                new discord.RichEmbed()
                    .setTitle("I couldn't find the quote.")
                    .setDescription("Check you're using the quote number from `,myquotes`")
                    .setColor(0x7ebdc3)
            )
        }
        if ( quote.member_id !== message.member.id ) {
            if (message.member.hasPermission("ADMINISTRATOR")) {
                await message.client.knex("quotes").where({ quote_id, guild_id }).delete()
                return await message.channel.send(
                    new discord.RichEmbed()
                        .setTitle("Removed that quote.")
                        .setDescription("(It wasn't your quote, but you're an admin so I removed it anyway)")
                        .setColor(0x7ebdc3)
                )
            }
            return await message.channel.send(
                new discord.RichEmbed()
                    .setTitle("That isn't your quote!")
                    .setDescription("Ask the owner or an administrator to remove it.")
                    .setColor(0x7ebdc3)
            )
        }
        await message.client.knex("quotes").where({ quote_id, guild_id }).delete()
        return await message.channel.send(
            new discord.RichEmbed()
                .setTitle("Removed!")
                .setColor(0x7ebdc3)
        )
    }
}
module.exports = RemovequoteCommand;
