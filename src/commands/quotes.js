const {
    Command
} = require('discord-akairo');

const discord = require("discord.js")
const Message = discord.Message
const { Embeds: EmbedsMode } = require('discord-paginationembed');
const { FieldsEmbed: FieldsEmbedMode } = require('discord-paginationembed');

class QuotesCommand extends Command {
    constructor() {
        super('quotes', {
            aliases: ['quotes'],
            userPermissions: "ADMINISTRATOR",
            category: "utility",
            channelRestriction: 'guild',
            args: [
                {
                    id: "staff",
                    type: "member"
                }
            ]
        });
    }
    
    /**
     * 
     * @param {Message} message Message sent
     * @param {*} param1 
     */
    async exec(message, { staff }) {
        if (!staff) {
            return await message.channel.send(
                new discord.RichEmbed()
                    .setTitle("I couldn't find that member.")
                    .setDescription("Please try using a mention")
                    .setColor(0x7ebdc3)
            )
        }

        var enable = await message.client.knex("enabled_staff").where({member_id: staff.id, guild_id: staff.guild.id}).first()
        if (!enable) {
            return await message.channel.send(
                new discord.RichEmbed()
                    .setTitle("They don't have quotes enabled!")
                    .setColor(0x7ebdc3)
            )
        }
        var quotes = await message.client.knex("quotes")
           .where({ member_id: staff.id, guild_id: staff.guild.id })
           .select()
        if (quotes.length == 0) return await message.channel.send(
            new discord.RichEmbed()
            .addField(staff.user.username + "'s quotes", "[none found]")
            .setColor(0x7ebdc3)
            .setTimestamp(new Date())
        )
        new FieldsEmbedMode()
           .setArray(quotes)
           .setAuthorizedUsers([message.author])
           .setChannel(message.channel)
           .setElementsPerPage(7)
           .setPage(1)
           .showPageIndicator(true)
           .setColor(0x7ebdc3)
           .setTimestamp(new Date())
           .formatField(staff.user.username + "'s quotes", i => `Quote ${i.quote_id} | ${i.quote.substring(0,100)}`)
           .build();

    }
}
module.exports = QuotesCommand;
