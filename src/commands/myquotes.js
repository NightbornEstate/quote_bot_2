const {
    Command
} = require('discord-akairo');

const discord = require("discord.js")
const Message = discord.Message
const { Embeds: EmbedsMode } = require('discord-paginationembed');
const { FieldsEmbed: FieldsEmbedMode } = require('discord-paginationembed');

class MyquotesCommand extends Command {
    constructor() {
        super('myquotes', {
            aliases: ['myquotes'],
            category: "utility",
            channelRestriction: 'guild'
        });
    }
    
    /**
     * 
     * @param {Message} message Message sent
     * @param {*} param1 
     */
    async exec(message, { quote }) {
        var enable = await message.client.knex("enabled_staff").where({member_id: message.member.id, guild_id: message.guild.id}).first()
        if (!enable) {
            return await message.channel.send(
                new discord.RichEmbed()
                    .setTitle("You don't have quotes enabled!")
                    .setDescription("Please ask an administrator.")
                    .setColor(0x7ebdc3)
            )
        }
        var quotes = await message.client.knex("quotes")
           .where({ member_id: message.member.id, guild_id: message.guild.id })
           .select()
        if (quotes.length == 0) return await message.channel.send(
            new discord.RichEmbed()
            .addField("Your quotes", "[none found]")
            .setColor(0x7ebdc3)
            .setTimestamp(new Date())
        )
        new FieldsEmbedMode({
            authorizedUsers: [message.author.id],
            channel: message.channel,
            clientMessage: { content: 'Preparing the embed... '},
            array: quotes,
            elementsPerPage: 10,
            pageIndicator: true,
            fields: [{name: 'Quote ID', value: i => `${i.quote_id}`}, { name: "Quote", value: i => `${i.quote.substring(1,100)}`}],
            page: 1,
            timeout: 69000
        })
           .setColor(0x7ebdc3)
           .setTimestamp(new Date())
           .build();

    }
}
module.exports = MyquotesCommand;
