const {
    Command
} = require('discord-akairo');

const discord = require("discord.js")
const Message = discord.Message

class DisablequotesCommand extends Command {
    constructor() {
        super('disablequotes', {
            channelRestriction: 'guild',
            aliases: ['disablequotes'],
            category: "utility",
            userPermissions: "ADMINISTRATOR",
            args: [{
                id: "staff",
                type: "member"
            }]
        });
    }
    
    /**
     * 
     * @param {Message} message Message sent
     * @param {*} param1 
     */
    async exec(message, { staff }) {
        if (!staff) {
            return await message.channel.send(
                new discord.RichEmbed()
                    .setTitle("I couldn't find that member.")
                    .setDescription("Please try using a mention")
                    .setColor(0x7ebdc3)
            )
        }
        var previous_enable = await message.client.knex("enabled_staff").where({member_id: staff.id, guild_id: message.guild.id}).first()
        if (!previous_enable) {
            return await message.channel.send(
                new discord.RichEmbed()
                    .setTitle("Member doesn't have quotes.")
                    .setDescription("Did you mean `,enablequotes`?")
                    .setColor(0x7ebdc3)
            )
        }
        await message.client.knex("enabled_staff")
            .where({ member_id: staff.id, guild_id: message.guild.id })
            .delete()
        return await message.channel.send(
            new discord.RichEmbed()
                .setTitle("Disabled quotes!")
                .setDescription("Quotes are no longer enabled for " + staff.user.username + ".")
                .setColor(0x7ebdc3)
        )
    }
}
module.exports = DisablequotesCommand;
