const {
    Command
} = require('discord-akairo');

const discord = require("discord.js")
const Message = discord.Message

class AddquoteCommand extends Command {
    constructor() {
        super('addquote', {
            aliases: ['addquote'],
            category: "utility",
            channelRestriction: 'guild',
            split: "none",
            args: [{
                id: "quote",
                type: "string"
            }]
        });
    }
    
    /**
     * 
     * @param {Message} message Message sent
     * @param {*} param1 
     */
    async exec(message, { quote }) {
        if (!quote) return await message.channel.send(
            new discord.RichEmbed()
                .setTitle("A quote is needed!")
                .setDescription("Usage: `,addquote <quote>`.")
                .setColor(0x7ebdc3)
        )
        var enable = await message.client.knex("enabled_staff").where({member_id: message.member.id, guild_id: message.guild.id}).first()
        if (!enable) {
            return await message.channel.send(
                new discord.RichEmbed()
                    .setTitle("You don't have quotes enabled!")
                    .setDescription("Please ask an administrator.")
                    .setColor(0x7ebdc3)
            )
        }
        await message.client.knex("quotes")
            .insert({ member_id: message.member.id, guild_id: message.guild.id, quote })
        await message.react("✅")
    }
}
module.exports = AddquoteCommand;
