var logger = require('winston');

//logger.add(logger.transports.File, { filename: "./logs/bot.log", level: "silly" });
//logger.remove(logger.transports.Console)
var l = new logger.transports.Console({
    level: "silly",
    format: logger.format.combine(logger.format.colorize(), logger.format.simple())
});
logger.add(l)
module.exports = logger;