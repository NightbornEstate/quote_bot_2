const { Listener } = require('discord-akairo');
const discord = require("discord.js");
const Message = discord.Message

class ReadyListener extends Listener {
    constructor() {
        super('message', {
            emitter: 'client',
            eventName: 'message'
        });
    }
    /**
     * Called on any message send
     * @param {Message} message The message sent
     */
    async exec(message) {
        if (message.author.bot) return // ignore self and other bots
        var trigger = message.content.split(" ") ? message.content.split(" ")[0].toLowerCase() : null;
        if (!trigger) return;
        var staff = await message.client.knex("enabled_staff").where({ guild_id: message.guild.id, trigger }).first()
        if (!staff) return
        var {member_id, guild_id} = staff 
        var member = message.guild.members.get(member_id)
        if (!member) return
        // Lookup quotes
        var q = await message.client.knex("quotes").where({ member_id, guild_id, deleted: 0 }).orderByRaw("RANDOM()").first()
        if (!q) return;
        await message.channel.send(q.quote + " - " + member.displayName)
    }
}

module.exports = ReadyListener;