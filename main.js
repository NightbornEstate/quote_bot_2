const { AkairoClient } = require('discord-akairo');
const logger = require("./src/logger")
var token = require("./data/config.json").token;

var knex = require('knex')({
    client: 'sqlite3',
    connection: {
      filename: "./data/db.sqlite3"
    }
});

const client = new AkairoClient({
    ownerID: '193053876692189184',
    prefix: ',',
    allowMention: true,
    commandDirectory: './src/commands/',
    inhibitorDirectory: './src/inhibitors/',
    listenerDirectory: './src/listeners/'
});
client.knex = knex
client.login(token).then( () => {
    logger.info("Logged in as " + client.user.id)
});
client.commandHandler.on("commandStarted", (message, command, edited) => {
    logger.info(`Command ${command.id} executed by ${message.author.tag} (${message.author.id}) in ${message.channel.name} with content ${message.content}`)
})